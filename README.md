# Amazon Sign-In and Sign-Up Test

**Pre-requisite**
----
- Ruby installed.
- Cucumber installed `gem install cucumber`
- Watir installed `gem install watir`
- Chrome web driver installed in Ruby path.

**How to run**
----
From root folder run:
```
cucumber
```

**Result**
----
![Result](result.png)
