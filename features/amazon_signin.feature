Feature: Amazon Sign-In

    Scenario Outline: Sign-in
        Given I have visited Amazon web
        When I click sign in
        Then sign-in page loaded successfully
        And I fill the sign-in form with <id> and expect <expected>
        And I click continue sign-in
        And verify password shown if true expected

        Examples:
        | id                | expected  |
        | mery@me           | false     |
        | 1                 | false     |
        | amazon@gmail.com  | true      |
 