Feature: Amazon Sign-Up

    Scenario: Sign-up success to OTP page
        Given I have visited Amazon web
        When I click sign up
        Then sign-up page loaded successfully
        And I fill the sign-up form with:
            | name  | email         | password  | retype    |
            | mery  | mery@mery.ph  | merymery  | merymery  |
        And I click submit sign-up
        And OTP page success load
    
    Scenario: Sign-up email already exist
        Given I have visited Amazon web
        When I click sign up
        Then sign-up page loaded successfully
        And I fill the sign-up form with:
            | name  | email             | password  | retype    |
            | mery  | amazon@gmail.com  | merymery  | merymery  |
        And I click submit sign-up
        And OTP page not load

    Scenario: Sign-up form validation
        Given I have visited Amazon web
        When I click sign up
        Then sign-up page loaded successfully
        And I fill the sign-up form with:
            | name  | email     | password  | retype    |
            |       | mery@me   | mery      | merr      |
        And I click submit sign-up
        And error messages shown
