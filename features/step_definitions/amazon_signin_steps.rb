When(/^I click sign in$/) do
    @browser.link(id: 'nav-link-accountList').hover
    @browser.div(id: 'nav-flyout-ya-signin').link.click
end

Then(/^sign-in page loaded successfully$/) do
    assert_equal(true, @browser.form(name: 'signIn').exists?)
end

And(/^I fill the sign-in form with (.*) and expect (.*)$/) do | id, expected |
    @browser.text_field(id: "ap_email").set(id)
    @expected = expected
end

And(/^I click continue sign-in$/) do
    @browser.form(name: 'signIn').submit
end

And(/^verify password shown if true expected$/) do
    assert_equal(@expected, @browser.text_field(id: 'ap_password').exists?.to_s)
end
