When(/^I click sign up$/) do
    @browser.link(id: 'nav-link-accountList').hover
    @browser.div(id: 'nav-flyout-ya-newCust').link.click
end

Then(/^sign-up page loaded successfully$/) do
    assert_equal(true, @browser.form(id: 'ap_register_form').exists?)
end

And(/^I fill the sign-up form with:$/) do |table|
    @data = table.hashes
    @browser.text_field(id: 'ap_customer_name').set(@data[0]['name'])
    @browser.text_field(id: 'ap_email').set(@data[0]['email'])
    @browser.text_field(id: 'ap_password').set(@data[0]['password'])
    @browser.text_field(id: 'ap_password_check').set(@data[0]['retype'])
end

And(/^I click submit sign-up$/) do 
    @browser.form(id: 'ap_register_form').submit
end

And(/^OTP page (.*) load$/) do |param|
    expected = param == 'success' ? true : false
    assert_equal(expected, @browser.div(id: 'cvf-page-content').exists?)
end

And(/^error messages shown$/) do
    assert_equal(true, @browser.div(id: 'auth-customerName-missing-alert').present?)
    assert_equal(true, @browser.div(id: 'auth-email-invalid-email-alert').present?)
    assert_equal(true, @browser.div(id: 'auth-password-invalid-password-alert').present?)
    assert_equal(true, @browser.div(id: 'auth-password-mismatch-alert').present?)
end